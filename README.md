url: http://twiliorest.herokuapp.com/api/sms

Twiliorest

dependencias
web.py==0.37
requests==2.5.3
twilio==3.7.2


web.py: framework web y conector a Postgresql  
run.py: archivo para iniciar el servicio / python run.py
model.py archivo que maneja consultas a la bd
sms.py archivo que maneja las funciones de la API de Twilio

Archivos de configuración
requeriments.txt contiene las dependencias de la aplicación
schema.sql contiene el script de la bd
Procfile archivo de configuración que indica la iniciación para Heroku
